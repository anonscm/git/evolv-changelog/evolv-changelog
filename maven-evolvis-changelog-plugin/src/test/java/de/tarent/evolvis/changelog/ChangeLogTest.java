package de.tarent.evolvis.changelog;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Ignore;

import javax.xml.rpc.ServiceException;

import junit.framework.TestCase;

public class ChangeLogTest extends TestCase {

	protected void setUp() throws Exception {
		super.setUp();

	}

	protected void tearDown() throws Exception {

		super.tearDown();
	}

	@Ignore /* This test does not work so far with the current version 4.8 of evolvis.org. 
	Please enable the test if the version of evolvis.org has changed to evolvis 5.1. */
	public void testEvolvisChangeLog() throws IOException, ServiceException {
		List<Tracker> trackers = new ArrayList<Tracker>();

		Map<String, String> releaseNotes = new HashMap<String, String>();
		releaseNotes.put("extrafeldauswahl", "Auswahl"); // type 1
		releaseNotes.put("extrafeldcheckbox", "Checkbox"); // type 2
		releaseNotes.put("extrafeldradio", "Radio"); // type 3
		releaseNotes.put("extrafeldtextfeld", "Textfeld"); // type 4 -> freitext
		releaseNotes.put("extrafeldmehrfach", "Mehrfach"); // type 5
		releaseNotes.put("extrafeldtextbereich", "Textbereich"); // type 6 ->
																	// freitext
		releaseNotes.put("extrafeldstatus", "Status"); // type 7

		Map<String, String> conditions = new HashMap<String, String>();

		trackers.add(new Tracker("JUnit Test Tracker", "JUnit Test Tracker", "extrafeldtextfeld",
				releaseNotes, conditions));
		List<String> versions = new ArrayList<String>();
		versions.add("Textfeld");
		Evolvis evolvis = new Evolvis("evolvis.org", "evolv-changelog", null,
				null, trackers);

		File changelogFile = File.createTempFile(
				"maven-evolvis-changelog-plugin-test", "txt");

		EvolvisChangelog changelog = new EvolvisChangelog();
		changelog.createChangeLog(evolvis, changelogFile.getParentFile(),
				changelogFile.getName(), "1.2.3", versions);

		InputStream isSoll = ClassLoader
				.getSystemResourceAsStream("junit-changelog.txt");
		InputStream isIst = new FileInputStream(changelogFile);

		assertTrue("Changelog not equal to template",
				compareInputStreams(isSoll, isIst));
	}

	private boolean compareInputStreams(InputStream isSoll, InputStream isIst)
			throws IOException {
		String soll = readInputStream(isSoll).trim();
		String ist = readInputStream(isIst).trim();
		return soll.equals(ist);
	}

	private String readInputStream(InputStream is) throws IOException {

		BufferedInputStream bis = new BufferedInputStream(is);

		StringBuilder sb = new StringBuilder();
		int i = bis.read();
		while (i >= 0) {
			sb.append((char) i);
			i = bis.read();
		}
		return sb.toString();
	}

}
