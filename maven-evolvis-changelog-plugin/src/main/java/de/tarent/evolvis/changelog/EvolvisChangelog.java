package de.tarent.evolvis.changelog;

/*
 * <pre>
 * How to debug Maven plugins:
 * export MAVEN_OPTS="-Xms256m -Xmx1500m -XX:PermSize=128m -XX:-UseGCOverheadLimit -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=y" 
 * mvn de.tarent.evolvis.changelog:evolvischangelog:changelog
 * 
 * In eclipse, create new empty project, 
 * Debug configurations, new Remote Java Application, add required sources, debug
 * </pre>
 * 
 * 
 */

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import org.evolvis.wsclient.EvolvisClient;
import org.evolvis.wsclient.EvolvisProject;
import org.evolvis.wsclient.EvolvisTracker;
import org.evolvis.wsclient.Issue;

import de.tarent.evolvis.changelog.output.ChangeLog;
import de.tarent.evolvis.changelog.output.ChangeLogCategory;
import de.tarent.evolvis.changelog.output.ChangeLogEntry;

/**
 * Evolvis Changelog
 * 
 */
public class EvolvisChangelog {

	/** Changelog */
	private ChangeLog changeLog;

	/** Evolvis */
	private Evolvis evolvis;

	/** versions to check in issue management */
	private List<String> versions;

	/**
	 * Create the change log
	 * 
	 * @param evolvis
	 *            evolvis configuration
	 * @param projectPath
	 *            directory for changelog
	 * @param changeLogFile
	 *            changelog name
	 * @param version
	 *            version
	 * @throws ServiceException
	 * @throws IOException
	 */
	public void createChangeLog(Evolvis ev, File projectPath,
			String changeLogFile, String version, List<String> vs)
			throws ServiceException, IOException {

		evolvis = ev;
		versions = vs;

		changeLog = new ChangeLog(new File(projectPath.getAbsolutePath()
				+ File.separator + changeLogFile), true, version);

		EvolvisClient evolvisClient = new EvolvisClient(evolvis.getInstance());
		evolvisClient.connect(evolvis.getUser(), evolvis.getPassword());

		EvolvisProject project = evolvisClient.getProject(evolvis.getProject());

		for (Tracker t : evolvis.getTrackers()) {
			EvolvisTracker tracker = project.getTracker(t.getName());
			handleIssues(t, tracker);
		}

		changeLog.write();
		evolvisClient.disconnect();

	}

	/**
	 * Iterate over issues in evolvisTracker, match given conditions and add
	 * entries to changelog
	 * 
	 * @param tracker
	 *            tracker information
	 * @param evolvisTracker
	 *            evolvis tracker object
	 * @throws RemoteException
	 */
	private void handleIssues(Tracker tracker, EvolvisTracker evolvisTracker)
			throws RemoteException {

		ChangeLogCategory category = new ChangeLogCategory(
				tracker.getDescription());

		for (Issue issue : evolvisTracker.getAllIssues()) {
			if (matchesVersion(tracker, issue)
					&& matchesConditions(issue, tracker.getConditions())) {
				addChangelogEntry(category, issue, tracker);
			}
		}

		if (!category.isEmpty()) {
			changeLog.addCategory(category);
		}
	}

	/**
	 * Tests if any of the specified versions match the issue
	 * 
	 * @param tracker
	 *            Tracker info
	 * @param issue
	 *            Evolvis issue
	 * @return true if any version matches
	 */
	private boolean matchesVersion(Tracker tracker, Issue issue) {
		for (String version : versions) {
			if (version.equals(issue.getExtraField(tracker.getVersionField()))) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Adds a changelog entry
	 * 
	 * @param category
	 *            changelog category
	 * @param issue
	 *            Evolvis issue
	 * @param tracker
	 *            tracker information
	 */
	private void addChangelogEntry(ChangeLogCategory category, Issue issue,
			Tracker tracker) {

		ChangeLogEntry entry = new ChangeLogEntry(category, issue.getSummary());

		Map<String, String> extraFields = tracker.getReleaseNotes();
		entry.setUrl(issue.getPermaLink());

		if (extraFields != null) {
			for (String fieldName : extraFields.keySet()) {
				entry.addAdditionalInfo(extraFields.get(fieldName),
						issue.getExtraField(fieldName));
			}
		}
		category.addEntry(entry);
	}

	/**
	 * Match given conditions on issue
	 * 
	 * @param issue
	 *            Evolvis issue
	 * @param conditions
	 *            Map of conditions. Key is the extra or special field name of
	 *            the evolvis issue, value is the value to match
	 * @return true if all conditions match
	 */
	private boolean matchesConditions(Issue issue,
			Map<String, String> conditions) {

		if (conditions != null) {
			for (String key : conditions.keySet()) {

				String fieldValue = issue.getExtraField(key);

				if (!conditions.get(key).equals(fieldValue)) {
					return false;
				}
			}
		}
		return true;
	}

}