package de.tarent.evolvis.changelog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;

/**
 * 
 * @goal changelog
 */
public class EvolvisChangelogMojo extends AbstractMojo {

	/**
	 * Maven project
	 * 
	 * @parameter default-value="${project}"
	 */
	private org.apache.maven.project.MavenProject mavenProject;

	/**
	 * version of current build
	 * 
	 * @parameter default-value="${project.version}"
	 */
	private String version;

	/**
	 * list of versions to check tracker issue against
	 * 
	 * @parameter
	 */
	private List<String> versions;

	/**
	 * Evolvis configuration
	 * 
	 * @parameter
	 */
	private Evolvis evolvis;

	/**
	 * Change-Log file name
	 * 
	 * @parameter default-value="CHANGELOG"
	 */
	private String changeLogFile;

	public void execute() throws MojoExecutionException, MojoFailureException {
		
		try {
			evolvis.setCredentialsFromConfigFile();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		if (versions == null) {
			versions = new ArrayList<String>();
		}

		if (!versions.contains(version)) {
			versions.add(version);
		}

		EvolvisChangelog ecl = new EvolvisChangelog();
		try {
			ecl.createChangeLog(evolvis, mavenProject.getBasedir(),
					changeLogFile, version, versions);
		} catch (Exception e) {
			throw new MojoExecutionException("FAIL", e);
		}
	}
}
