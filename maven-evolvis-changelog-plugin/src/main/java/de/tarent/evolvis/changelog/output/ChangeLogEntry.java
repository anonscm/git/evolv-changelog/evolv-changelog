package de.tarent.evolvis.changelog.output;
import java.util.ArrayList;
import java.util.List;

public class ChangeLogEntry {

    /** change log category */
    private ChangeLogCategory category;
    
    /** summary */
    private String summary;
    
    /** List of additinal information */
    private List<String[]> additionalInfo;
    
    /** URL */
    private String url;

    /**
     * Creates a new change log entry 
     * @param category category
     * @param summary summary
     */
    public ChangeLogEntry(ChangeLogCategory category, String summary) {
	this.category = category;
	this.summary = summary;
	additionalInfo = new ArrayList<String[]>();
    }

    /**
     * Adds additional information
     * @param field information description
     * @param value information value
     */
    public void addAdditionalInfo(String field, String value) {
	additionalInfo.add(new String[] { field, value });
    }

    public void setUrl(String url) {
	this.url = url;
    }

    public ChangeLogCategory getCategory() {
	return category;
    }

    public String getSummary() {
	return summary;
    }

    public List<String[]> getAdditionalInfo() {
	return additionalInfo;
    }

    public String getUrl() {
	return url;
    }

    public String toString() {
	StringBuilder s = new StringBuilder();

	s.append(summary);
	if (url != null) {
	    s.append("\n  ").append("URL: ").append(url);
	}

	for (String[] a : additionalInfo) {
	    s.append("\n  ").append(a[0]).append(" : ").append(a[1]);
	}
	
	return s.toString();
    }

}
