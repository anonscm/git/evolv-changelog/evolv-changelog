package de.tarent.evolvis.changelog;

import java.util.Map;

/**
 * Evolvis Tracker. Used for maven plugin configuration
 * 
 */
public class Tracker {

	/** tracker name */
	private String name;

	/** tracker description, printed in changelog */
	private String description;

	/**
	 * extrafields of Tracker items to include in changelog. key is extra-field
	 * alias, value is description printed in changelog
	 */
	private Map<String, String> releaseNotes;

	/**
	 * Conditions to check. Key is extra-field alias, value is required value of
	 * extra-field
	 */
	private Map<String, String> conditions;

	/** field name which stores fixed in version information */
	private String versionField;
	
	public Tracker() {

	}

	/**
	 * Creates new tracker
	 * 
	 * @param n
	 *            name
	 * @param d
	 *            description
	 * @param r
	 *            releasenotes
	 * @param c
	 *            conditions
	 */
	public Tracker(String n, String d, String v, Map<String, String> r,
			Map<String, String> c) {
		this.name = n;
		this.description = d;
		this.versionField = v;
		this.releaseNotes = r;
		this.conditions = c;
	}

	/**
	 * get name
	 * 
	 * @return name of evolvis tracker
	 */
	public String getName() {
		return name;
	}

	/**
	 * additional releasenotes
	 * 
	 * @return release notes
	 */
	public Map<String, String> getReleaseNotes() {
		return releaseNotes;
	}

	/**
	 * conditions
	 * 
	 * @return conditions
	 */
	public Map<String, String> getConditions() {
		return conditions;
	}

	/**
	 * get description
	 * 
	 * @return description to be printed in changelog
	 */
	public String getDescription() {
		return description;
	}

	public String getVersionField() {
		return versionField;
	}
	
}
