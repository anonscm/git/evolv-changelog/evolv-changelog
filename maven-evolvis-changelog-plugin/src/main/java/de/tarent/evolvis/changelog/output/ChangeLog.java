package de.tarent.evolvis.changelog.output;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Changelog class. Collects changelog entries and writes them into a file
 * 
 */
public class ChangeLog {

	/** changelog file */
	private File changeLog;

	/** flag if new changelog shall be prepended to existing log */
	private boolean prepend;

	/** current version */
	private String version;

	/** List of categories */
	private List<ChangeLogCategory> categories;

	/**
	 * Creates new changelog
	 * 
	 * @param changeLogFile
	 *            changelog file
	 * @param prepend
	 *            flag to prepend new changelog
	 * @param version 
	 */
	public ChangeLog(File changeLogFile, boolean prepend, String version) {
		this.changeLog = changeLogFile;
		this.prepend = prepend;
		this.version = version;
		categories = new ArrayList<ChangeLogCategory>();
	}

	/**
	 * Adds a new category
	 * 
	 * @param category
	 *            category to add
	 */
	public void addCategory(ChangeLogCategory category) {
		categories.add(category);
	}

	/**
	 * write changelog to file
	 * 
	 * @throws IOException
	 */
	public void write() throws IOException {
		String tail = null;

		if (prepend && changeLog.exists()) {
			// read existing changelog
			StringBuffer oldLog = new StringBuffer();
			BufferedReader reader = new BufferedReader(
					new FileReader(changeLog));
			String line = reader.readLine();
			while (line != null) {
				oldLog.append(line);
				oldLog.append("\n");
				line = reader.readLine();
			}
			reader.close();
			tail = oldLog.toString();

			changeLog.delete();
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(changeLog));

		writeChangeLog(writer);

		if (prepend && tail != null) {
			// append existing changelog
			writer.write(tail);
		}

		writer.close();
	}

	/**
	 * Write the new changelog entries into the writer
	 * 
	 * @param writer
	 *            writer
	 * @throws IOException
	 */
	private void writeChangeLog(BufferedWriter writer) throws IOException {

		writer.write("=== ");
		writer.write(version);
		writer.write(" ===");
		writer.write("\n\n");
		
		for (ChangeLogCategory category : categories) {
			writer.write(category.toString());
		}

	}

}
