package de.tarent.evolvis.changelog.output;

import java.util.ArrayList;
import java.util.List;

public class ChangeLogCategory {

	private String category;

	private List<ChangeLogEntry> entries;

	public ChangeLogCategory(String category) {
		this.category = category;
		entries = new ArrayList<ChangeLogEntry>();
	}

	public String getCategory() {
		return category;
	}

	public List<ChangeLogEntry> getEntries() {
		return entries;
	}

	public void addEntry(ChangeLogEntry entry) {
		entries.add(entry);
	}

	public String toString() {
		StringBuilder s = new StringBuilder();

		s.append(category).append("\n");

		for (ChangeLogEntry e : entries) {
			s.append(e.toString());
			s.append("\n");
		}

		s.append("\n");

		return s.toString();
	}

	public boolean isEmpty() {
		return entries.isEmpty();
	}
}
