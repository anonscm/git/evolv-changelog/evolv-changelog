package de.tarent.evolvis.changelog;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Evolvis configuration object. Used for maven plugin configuration
 * 
 */
public class Evolvis {
	
	private Logger log = Logger.getLogger("Evolvis");

	/** Evolvis domain */
	private String instance;

	/** project unix name */
	private String project;

	/** evolvis username */
	private String username;

	/** evolvis password */
	private String password;
	
	/** path to the configuration file of evolvis credentials */
	private String propertiesPath;
	
	/** path to an alternative configuration file with evolvis credentials */
	private static final String alternativePropertiesPath ="/opt/evolvis.properties";

	/** List of Tracker */
	private List<Tracker> trackers;

	public Evolvis() {

	}

	/**
	 * Creates new object
	 * 
	 * @param i
	 *            instance
	 * @param p
	 *            project
	 * @param u
	 *            username, use null for anonymous login
	 * @param pw
	 *            password, ignored for anonymous login
	 * @param t
	 *            List of Tracker
	 */
	public Evolvis(String i, String p, String u, String pw, List<Tracker> t) {
		this.instance = i;
		this.project = p;
		this.username = u;
		this.password = pw;
		this.trackers = t;
	}
	
	/**
	 * get evolvis domain
	 * 
	 * @return evolvis domain
	 */
	public String getInstance() {
		return instance;
	}

	/**
	 * get project unix name
	 * 
	 * @return project unix name
	 */
	public String getProject() {
		return project;
	}

	/**
	 * get username
	 * 
	 * @return username
	 */
	public String getUser() {
		return username;
	}

	/**
	 * get password
	 * 
	 * @return password
	 */
	public String getPassword() {
		return password;
	}
	
	/**
	 * get propertiesPath
	 * 
	 * @return propertiesPath
	 */
	public String getPropertiesPath() {
		return propertiesPath;
	}
	
	/**
	 * get propertiesPath
	 * 
	 * @return propertiesPath
	 */
	private String getAlternativePropertiesPath() {
		return alternativePropertiesPath;
	}

	/**
	 * get List of Tracker
	 * 
	 * @return List of Tracker
	 */
	public List<Tracker> getTrackers() {
		return trackers;
	}
	
	private Properties getConfigFileProperties(String propertiesPath) {
		Properties properties = new Properties();
		
		BufferedInputStream stream = null;
		
		try {
			
			stream = new BufferedInputStream(new FileInputStream(propertiesPath));
			properties.load(stream);
			stream.close();
			
			log.info("The data file including the evolvis credentials was found.");
			
		} catch(FileNotFoundException e) {
			log.info("The data file including the evolvis credentials was not found under the following path: "+propertiesPath);
		} catch (Exception e) {
			log.info("An unknown error occured.");
		}
			
		return properties;
	}
	
	/**
	 * void setCredentialsFromConfigFile()
	 * 
	 * tries to set evolvis user and password from a properties file
	 * in propertiesPath
	 * 
	 * @throws IOException
	 */
	public void setCredentialsFromConfigFile() throws IOException {
		
		Properties properties = new Properties();
		
		properties = getConfigFileProperties(getPropertiesPath());
		if(properties.isEmpty()) {
			properties = getConfigFileProperties(getAlternativePropertiesPath());
		}
		
		if(!properties.isEmpty()) {
			
			username = properties.getProperty("username");
			password = properties.getProperty("password");
		
			if(username == null || username.toString().trim().equals("")) {
				log.info("No username available in the properties data file.");
			}
		
			if(password == null || password.toString().trim().equals("")) {
				log.info("No password available in the properties data file.");
			}
		}
	}
}